package com.example.syllabusrepository.converter;

import com.example.syllabusrepository.dto.DaySyllabusDto;
import com.example.syllabusrepository.dto.TrainingContentDto;
import com.example.syllabusrepository.dto.TrainingUnitListDto;
import com.example.syllabusrepository.entity.DaySyllabus;
import com.example.syllabusrepository.entity.TrainingContent;
import com.example.syllabusrepository.entity.TrainingUnit;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component
public class DaySyllabusConverter {
    public static DaySyllabusDto toDTO(DaySyllabus entity){
        DaySyllabusDto dto = new DaySyllabusDto();
        dto.setId(entity.getId());
        dto.setDay(entity.getDay());
        dto.setCreatedDate(entity.getCreatedDate());
        dto.setCreatedBy(entity.getCreatedBy());
        dto.setModifyDate(entity.getCreatedDate());
        dto.setModifyBy(entity.getModifyBy());
        //dto.setTrainingUnitListDtos(entity.getTrainingUnit());
        return dto;
    }


    public static DaySyllabus toEntity(DaySyllabusDto dto){
        DaySyllabus entity = new DaySyllabus();
        //entity.setId(dto.getId());
        entity.setDay(dto.getDay());
        entity.setCreatedDate(LocalDate.now());
        entity.setCreatedBy(dto.getCreatedBy());
        entity.setModifyDate(LocalDate.now());
        entity.setModifyBy(dto.getModifyBy());
        return entity;
    }
    public static DaySyllabusDto toDTOCreate(DaySyllabus entity) {
        DaySyllabusDto dto = new DaySyllabusDto();
        dto.setId(entity.getId());
        dto.setDay(entity.getDay());
        dto.setCreatedDate(entity.getCreatedDate());
        dto.setCreatedBy(entity.getCreatedBy());
        dto.setModifyDate(entity.getCreatedDate());
        dto.setModifyBy(entity.getModifyBy());

        List<TrainingUnit> trainingUnits = entity.getTrainingUnit();
        List<TrainingUnitListDto> trainingUnitListDtos = new ArrayList<>();
        for (TrainingUnit trainingUnit : trainingUnits) {
            TrainingUnitListDto trainingUnitListDto = new TrainingUnitListDto();
            trainingUnitListDto.setId(trainingUnit.getId());
            //trainingUnitListDto.setDayNumber(trainingUnit.getDayNumber());
            trainingUnitListDto.setUnitName(trainingUnit.getUnitName());
            trainingUnitListDto.setUnitNumber(trainingUnit.getUnitNumber());
            trainingUnitListDto.setCreatedDate(trainingUnit.getCreatedDate());
            trainingUnitListDto.setCreatedBy(trainingUnit.getCreatedBy());
            trainingUnitListDto.setModifyDate(trainingUnit.getModifyDate());
            trainingUnitListDto.setModifyBy(trainingUnit.getModifyBy());

            // Thêm danh sách TrainingContentDto vào TrainingUnitListDto
            List<TrainingContentDto> trainingContentDtos = new ArrayList<>();
            for (TrainingContent content : trainingUnit.getTrainingContents()) {
                TrainingContentDto contentDto = new TrainingContentDto();
                contentDto.setName(content.getName());
                contentDto.setDeliveryType(content.getDeliveryType());
                contentDto.setOutputStandard(content.getOutputStandard());
                contentDto.setTrainingTime(content.getTrainingTime());
                contentDto.setMethod(content.getMethod());
                contentDto.setId(content.getId());
                contentDto.setCreatedDate(content.getCreatedDate());
                contentDto.setCreatedBy(content.getCreatedBy());
                contentDto.setModifyDate(content.getModifyDate());
                contentDto.setModifyBy(content.getModifyBy());
                trainingContentDtos.add(contentDto);
            }
            trainingUnitListDto.setTrainingContent(trainingContentDtos);

            trainingUnitListDtos.add(trainingUnitListDto);
        }
        dto.setTrainingUnitListDtos(trainingUnitListDtos);

        return dto;
    }



    public static DaySyllabus toEntity(DaySyllabusDto dto, DaySyllabus entity){
        //entity.setId(dto.getId());
        entity.setDay(dto.getDay());
        entity.setCreatedDate(LocalDate.now());
        entity.setCreatedBy(dto.getCreatedBy());
        entity.setModifyDate(LocalDate.now());
        entity.setModifyBy(dto.getModifyBy());
        return entity;
    }
}
