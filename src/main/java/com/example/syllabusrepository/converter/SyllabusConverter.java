package com.example.syllabusrepository.converter;

import com.example.syllabusrepository.dto.SyllabusDto;
import com.example.syllabusrepository.entity.Syllabus;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class SyllabusConverter {
    public static SyllabusDto toDTO(Syllabus entity) {
        SyllabusDto dto = new SyllabusDto();
        dto.setId(entity.getId());
        dto.setSyllabusName(entity.getSyllabusName());
        dto.setSyllabusCode(entity.getSyllabusCode());
        dto.setVersion(entity.getVersion());
        dto.setAttendeeNumber(entity.getAttendeeNumber());
        dto.setDuration(entity.getDuration());
        dto.setTechnicalRequirement(entity.getTechnicalRequirement());
        dto.setOutputStandard(entity.getOutputStandard());
        dto.setCourseObjectives(entity.getCourseObjectives());
        dto.setLevel(entity.getLevel());
        dto.setPublicStatus(entity.getPublicStatus());
        dto.setTrainingId(entity.getTrainingId());
        dto.setCreatedDate(entity.getCreatedDate());
        dto.setCreatedBy(entity.getCreatedBy());
        dto.setModifyDate(entity.getModifyDate());
        dto.setModifyBy(entity.getModifyBy());
        return dto;
    }

    public static Syllabus toEntity(SyllabusDto dto) {
        Syllabus entity = new Syllabus();
        //entity.setId(dto.getId());
        entity.setSyllabusName(dto.getSyllabusName());
        entity.setSyllabusCode(dto.getSyllabusCode());
        entity.setVersion(dto.getVersion());
        entity.setAttendeeNumber(dto.getAttendeeNumber());
        entity.setDuration(dto.getDuration());
        entity.setTechnicalRequirement(dto.getTechnicalRequirement());
        entity.setOutputStandard(dto.getOutputStandard());
        entity.setCourseObjectives(dto.getCourseObjectives());
        entity.setLevel(dto.getLevel());
        entity.setPublicStatus(dto.getPublicStatus());
        entity.setTrainingId(dto.getTrainingId());
        //entity.setPublicStatus(dto.getPublicStatus());
        entity.setCreatedDate(LocalDate.now());
        entity.setCreatedBy(dto.getCreatedBy());
        entity.setModifyDate(LocalDate.now());
        entity.setModifyBy(dto.getModifyBy());
        return entity;
    }

    public static Syllabus toEntity(SyllabusDto dto, Syllabus entity) {
        //entity.setId(dto.getId());
        entity.setSyllabusName(dto.getSyllabusName());
        entity.setSyllabusCode(dto.getSyllabusCode());
        entity.setVersion(dto.getVersion());
        entity.setAttendeeNumber(dto.getAttendeeNumber());
        entity.setDuration(dto.getDuration());
        entity.setTechnicalRequirement(dto.getTechnicalRequirement());
        entity.setOutputStandard(dto.getOutputStandard());
        entity.setCourseObjectives(dto.getCourseObjectives());
        entity.setLevel(dto.getLevel());
        entity.setPublicStatus(dto.getPublicStatus());
        entity.setTrainingId(dto.getTrainingId());
        if (entity.getCreatedDate() == null) {
            entity.setCreatedDate(dto.getCreatedDate());
        }
        entity.setCreatedBy(dto.getCreatedBy());
        entity.setModifyDate(LocalDate.now());
        entity.setModifyBy(dto.getModifyBy());
        return entity;
    }
}
