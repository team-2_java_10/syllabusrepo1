package com.example.syllabusrepository.converter;

import com.example.syllabusrepository.dto.AssignmentShemeDto;
import com.example.syllabusrepository.entity.AssignmentSheme;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class AssignmentShemeConverter {
    public static AssignmentShemeDto toDTO(AssignmentSheme entity){
        AssignmentShemeDto dto = new AssignmentShemeDto();
        dto.setId(entity.getId());
        dto.setAssignment(entity.getAssignment());
        dto.setQuiz(entity.getQuiz());
        dto.setFinalSyllabus(entity.getFinalSyllabus());
        dto.setFinalTheory(entity.getFinalTheory());
        dto.setFinalPractice(entity.getFinalPractice());
        dto.setGpa(entity.getGpa());
        dto.setCreatedDate(LocalDate.now());
        dto.setCreatedBy(entity.getCreatedBy());
        dto.setModifyDate(LocalDate.now());
        dto.setModifyBy(entity.getModifyBy());
        return dto;
    }

    public static AssignmentSheme toEntity(AssignmentShemeDto dto){
        AssignmentSheme entity = new AssignmentSheme();
        entity.setAssignment(dto.getAssignment());
        entity.setQuiz(dto.getQuiz());
        entity.setFinalSyllabus(dto.getFinalSyllabus());
        entity.setFinalTheory(dto.getFinalTheory());
        entity.setFinalPractice(dto.getFinalPractice());
        entity.setGpa(dto.getGpa());
        entity.setCreatedDate(LocalDate.now());
        entity.setCreatedBy(dto.getCreatedBy());
        entity.setModifyDate(LocalDate.now());
        entity.setModifyBy(dto.getModifyBy());
        return entity;
    }
    public static AssignmentSheme toEntity(AssignmentShemeDto dto, AssignmentSheme entity){
        entity.setAssignment(dto.getAssignment());
        entity.setQuiz(dto.getQuiz());
        entity.setFinalSyllabus(dto.getFinalSyllabus());
        entity.setFinalTheory(dto.getFinalTheory());
        entity.setFinalPractice(dto.getFinalPractice());
        entity.setGpa(dto.getGpa());
        entity.setCreatedDate(LocalDate.now());
        entity.setCreatedBy(dto.getCreatedBy());
        entity.setModifyDate(LocalDate.now());
        entity.setModifyBy(dto.getModifyBy());
        return entity;
    }
}
