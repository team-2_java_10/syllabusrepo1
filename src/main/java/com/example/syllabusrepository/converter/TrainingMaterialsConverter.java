package com.example.syllabusrepository.converter;

import com.example.syllabusrepository.dto.TrainingMaterialsDto;
import com.example.syllabusrepository.entity.TrainingMaterials;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class TrainingMaterialsConverter {
    public static TrainingMaterialsDto toDTO(TrainingMaterials entity){
        TrainingMaterialsDto dto = new TrainingMaterialsDto();
        dto.setId(entity.getId());
        dto.setTitle(entity.getTitle());
//        dto.setDescription(entity.getDescription());
//        dto.setStatus(entity.getStatus());
//        dto.setFilename(entity.getFilename());
        dto.setS3Location(entity.getS3Location());
        dto.setCreatedDate(LocalDate.now());
        dto.setCreatedBy(entity.getCreatedBy());
        dto.setModifyDate(LocalDate.now());
        dto.setModifyBy(dto.getModifyBy());
        return dto;
    }


}
