package com.example.syllabusrepository.converter;

import com.example.syllabusrepository.dto.OtherSyllabusDto;
import com.example.syllabusrepository.entity.OtherSyllabus;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class OtherSyllabusConverter {
    public static OtherSyllabusDto toDTO(OtherSyllabus entity){
        OtherSyllabusDto dto = new OtherSyllabusDto();
        dto.setId(entity.getId());
        dto.setTrainingPriciples(entity.getTrainingPriciples());
        dto.setCreatedDate(LocalDate.now());
        dto.setCreatedBy(entity.getCreatedBy());
        dto.setModifyDate(LocalDate.now());
        dto.setModifyBy(entity.getModifyBy());
        return dto;
    }
    public static OtherSyllabus toEntity(OtherSyllabusDto dto){
        OtherSyllabus entity = new OtherSyllabus();
        //entity.setId(dto.getId());
        entity.setTrainingPriciples(dto.getTrainingPriciples());
        entity.setCreatedDate(LocalDate.now());
        entity.setCreatedBy(dto.getCreatedBy());
        entity.setModifyDate(LocalDate.now());
        entity.setModifyBy(dto.getModifyBy());
        return entity;
    }
    public static OtherSyllabus toEntity(OtherSyllabusDto dto, OtherSyllabus entity){
        //entity.setId(dto.getId());
        entity.setTrainingPriciples(dto.getTrainingPriciples());
        entity.setCreatedDate(LocalDate.now());
        entity.setCreatedBy(dto.getCreatedBy());
        entity.setModifyDate(LocalDate.now());
        entity.setModifyBy(dto.getModifyBy());
        return entity;
    }
}
