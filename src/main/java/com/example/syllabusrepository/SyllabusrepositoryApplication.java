package com.example.syllabusrepository;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SyllabusrepositoryApplication {

    public static void main(String[] args) {
        SpringApplication.run(SyllabusrepositoryApplication.class, args);
    }

}
