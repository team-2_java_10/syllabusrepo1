package com.example.syllabusrepository.repository;

import com.example.syllabusrepository.entity.AssignmentSheme;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AssignmentShemeRepository extends JpaRepository<AssignmentSheme, Long> {
    List<AssignmentSheme> findBySyllabusId(Long id);
}
