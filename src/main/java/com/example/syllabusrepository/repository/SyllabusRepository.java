package com.example.syllabusrepository.repository;

import com.example.syllabusrepository.entity.Syllabus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SyllabusRepository extends JpaRepository<Syllabus, Long> {
    Optional<Syllabus> findBySyllabusCode(String topicName);
    Page<Syllabus> findAll(Specification<Syllabus> spec, Pageable pageable);
    boolean existsBySyllabusName(String syllabusName);
    //List<Syllabus> findBySyllabusId(Long id);
}
