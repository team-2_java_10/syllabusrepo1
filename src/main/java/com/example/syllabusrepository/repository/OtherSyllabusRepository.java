package com.example.syllabusrepository.repository;

import com.example.syllabusrepository.entity.OtherSyllabus;
import com.example.syllabusrepository.entity.Syllabus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface OtherSyllabusRepository extends JpaRepository<OtherSyllabus, Long> {
    List<OtherSyllabus> findBySyllabusId(Long id);
    Optional<OtherSyllabus> findByTrainingPriciples(String findByTrainingPriciples);

    Optional<OtherSyllabus> findBySyllabus(Syllabus syllabus);
}
