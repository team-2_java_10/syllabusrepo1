package com.example.syllabusrepository.repository;

import com.example.syllabusrepository.entity.TimeAllocation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TimeAllocationRepositoty extends JpaRepository<TimeAllocation, Long> {
    List<TimeAllocation> findBySyllabusId(Long id);
}
