package com.example.syllabusrepository.repository;

import com.example.syllabusrepository.entity.TrainingContent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TrainingContentRepository extends JpaRepository<TrainingContent, Long> {
    Optional<TrainingContent> findByName(String content);
}
