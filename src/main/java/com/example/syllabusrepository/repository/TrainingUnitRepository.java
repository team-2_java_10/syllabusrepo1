package com.example.syllabusrepository.repository;

import com.example.syllabusrepository.entity.TrainingUnit;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface TrainingUnitRepository extends JpaRepository<TrainingUnit, Long> {
    Optional<TrainingUnit> findByUnitName(String unitName);
    List<TrainingUnit> findByDaySyllabusId(Long id);
}
