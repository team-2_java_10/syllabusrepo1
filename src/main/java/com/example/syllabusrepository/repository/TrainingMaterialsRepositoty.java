package com.example.syllabusrepository.repository;

import com.example.syllabusrepository.entity.TrainingMaterials;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TrainingMaterialsRepositoty extends JpaRepository<TrainingMaterials, Long> {
}
