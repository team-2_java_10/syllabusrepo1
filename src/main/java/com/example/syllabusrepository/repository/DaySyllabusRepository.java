package com.example.syllabusrepository.repository;

import com.example.syllabusrepository.entity.DaySyllabus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface DaySyllabusRepository extends JpaRepository<DaySyllabus, Long> {
    Optional<DaySyllabusRepository> findByDay(String day);
    List<DaySyllabus> findBySyllabusId(Long id);
}
