package com.example.syllabusrepository.exceptions;

public class TrainingUnitNotFoundException extends RuntimeException{
    public TrainingUnitNotFoundException(String message){
        super(message);
    }
}
