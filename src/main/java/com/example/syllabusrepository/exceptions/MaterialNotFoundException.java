package com.example.syllabusrepository.exceptions;

public class MaterialNotFoundException extends RuntimeException{
    public MaterialNotFoundException(String message){
        super(message);
    }
}
