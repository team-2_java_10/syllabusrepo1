package com.example.syllabusrepository.exceptions;

import lombok.Data;

@Data
public class ResourceNotFoundException extends Exception{

    public ResourceNotFoundException(String message){
        super(message);
    }
}
