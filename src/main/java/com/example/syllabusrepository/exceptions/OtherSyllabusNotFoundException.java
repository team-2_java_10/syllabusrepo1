package com.example.syllabusrepository.exceptions;

public class OtherSyllabusNotFoundException extends RuntimeException {
    public OtherSyllabusNotFoundException(String message) {
        super(message);
    }
}
