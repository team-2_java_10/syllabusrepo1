package com.example.syllabusrepository.exceptions;

public class SyllabusNotFoundException extends RuntimeException{
    public SyllabusNotFoundException(String message){
        super(message);
    }
}
