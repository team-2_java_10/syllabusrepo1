package com.example.syllabusrepository.exceptions;

public class DaySyllabusNotFoundException extends RuntimeException{
    public DaySyllabusNotFoundException(String message){
        super(message);
    }

}
