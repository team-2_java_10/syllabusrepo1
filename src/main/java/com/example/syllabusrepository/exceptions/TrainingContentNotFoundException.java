package com.example.syllabusrepository.exceptions;

public class TrainingContentNotFoundException extends RuntimeException{
    public TrainingContentNotFoundException(String message){
        super(message);
    }
}
