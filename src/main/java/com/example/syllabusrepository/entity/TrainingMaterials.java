package com.example.syllabusrepository.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "TrainingMaterials")
public class TrainingMaterials extends BaseEntity{
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "materialsId")
//    private Long materialsId;
    @Column(name = "title", length = 500)
    private String title;

    //@Column(name = "createdBy",nullable = false, length = 500)
    //private String createdBy;

    private String s3Location;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    private byte[] data;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "contentId")
    private TrainingContent trainingContent;
}
