package com.example.syllabusrepository.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "assignmentSheme")
public class AssignmentSheme extends BaseEntity{
    @Column(name = "assignment", nullable = false, length = 500)
    private int assignment;
    @Column(name = "quiz", nullable = false, length = 500)
    private int quiz;
    @Column(name = "finalSyllabus", nullable = false, length = 500)
    private int finalSyllabus;
    @Column(name = "finalTheory", nullable = false, length = 500)
    private int finalTheory;
    @Column(name = "finalPractice", nullable = false, length = 500)
    private int finalPractice;
    @Column(name = "gpa", nullable = false, length = 500)
    private int gpa;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "syllabus_id", referencedColumnName = "id")
    private Syllabus syllabus;

}
