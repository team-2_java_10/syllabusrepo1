package com.example.syllabusrepository.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "trainingProgram")
public class TrainingProgram extends BaseEntity{
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "id", nullable = false)
//    private Long id;
    //private Long userId;
    //private String trainingProgramCode;
    @Column(name = "name", nullable = false, length = 500)
    private String name;
    @Column(name = "duration", nullable = false, length = 500)
    private String duration;
    @Column(name = "topicCode", nullable = false, length = 500)
    private String topicCode;
    @Column(name = "status", nullable = false)
    private int status;


/*    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;*/

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "trainingProgram_syllabuses", joinColumns = @JoinColumn(name = "training_program_code", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "topic_code", referencedColumnName = "id"))
    private List<Syllabus> syllabusList = new ArrayList<>();

}
