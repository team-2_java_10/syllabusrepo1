package com.example.syllabusrepository.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "OrderSyllabus")
public class OtherSyllabus extends BaseEntity{
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;
    private String trainingPriciples;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "syllabusId")
    private Syllabus syllabus;
}
