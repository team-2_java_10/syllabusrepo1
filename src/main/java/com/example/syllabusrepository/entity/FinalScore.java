package com.example.syllabusrepository.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "finalScore")
public class FinalScore extends BaseEntity {
//    //check unit
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "finalId", nullable = false, length = 500)
//    private Integer finalId;
    @Column(name = "plusScore", nullable = false, length = 500)
    private double plusScore;
    @Column(name = "assignment", nullable = false, length = 500)
    private String assignment;
    @Column(name = "quiz", nullable = false, length = 500)
    private String quiz;

/*    @OneToOne(mappedBy = "finalScore", cascade = CascadeType.ALL)
    private Syllabus syllabus;*/

}
