package com.example.syllabusrepository.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "timeAllocation")
public class TimeAllocation extends BaseEntity{
    @Column(name = "assignment", nullable = false, length = 500)
    private int assignment;
    @Column(name = "concept", nullable = false, length = 500)
    private int concept;
    @Column(name = "gulde", nullable = false, length = 500)
    private int gulde;
    @Column(name = "test", nullable = false, length = 500)
    private int test;
    @Column(name = "exam", nullable = false, length = 500)
    private int exam;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "syllabus_id", referencedColumnName = "id")
    private Syllabus syllabus;
}
