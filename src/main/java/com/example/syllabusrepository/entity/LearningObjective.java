package com.example.syllabusrepository.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "LearningObjective")
public class LearningObjective extends BaseEntity{
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "id")
//    private Long learningObjectiveId;
    //private String code;
    @Column(name = "name", nullable = false, length = 500)
    private String name;
    @Column(name = "type", nullable = false, length = 500)
    private String type;
    @Column(name = "description", nullable = false, length = 500)
    private String description;

/*    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "content_id")
    private TrainingContent trainingContent;*/
}
