package com.example.syllabusrepository.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "syllabus")
public class Syllabus extends BaseEntity{
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "id", nullable = false, length = 50)
//    private Long id;
    //private String topicCode;
    @Column(name = "syllabusName", nullable = false, length = 500)
    private String syllabusName; // syllabus name
    @Column(name = "syllabusCode", nullable = false, length = 500)
    private String syllabusCode;// technical requirement
    @Column(name = "duration", nullable = false, length = 500)
    private int duration;
    @Column(name = "version", nullable = false, length = 500)
    private String version;
    @Column(name = "attendeeNumber")
    private int attendeeNumber;
    @Column(name = "technicalRequirement", columnDefinition = "TEXT")
    private String technicalRequirement;
    @Column(name = "outputStandard", nullable = false, length = 500)
    private String outputStandard;
    @Column(name = "courseObjectives", columnDefinition = "TEXT")
    private String courseObjectives;
    @Column(name = "level", nullable = false, length = 500)
    private String level;
    @Column(name = "status", nullable = false, length = 500)
    // tu tu su ly
    public String publicStatus;
    @Column(name = "trainingId", length = 500)
    public Integer trainingId;

/*    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;*/

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "syllabus_objective", joinColumns = @JoinColumn(name = "syllabus_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "objecttive_id", referencedColumnName = "id"))
    private List<LearningObjective> learningObjectives = new ArrayList<>();


    @OneToMany(mappedBy = "syllabus", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<DaySyllabus> daSyllabus = new ArrayList<>();

    @OneToMany(mappedBy = "syllabus", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<OtherSyllabus> otherSyllabi = new ArrayList<>();

    @OneToOne(mappedBy = "syllabus")
    private TimeAllocation timeAllocation;

    @OneToOne(mappedBy = "syllabus")
    private AssignmentSheme assignmentSheme;

/*    @OneToOne
    @JoinColumn(name = "test_id")
    private Test test;

    @OneToOne
    @JoinColumn(name = "final_id")
    private FinalScore finalScore;*/
    
}
