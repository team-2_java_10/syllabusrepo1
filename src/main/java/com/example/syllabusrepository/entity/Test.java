package com.example.syllabusrepository.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "test")
public class Test extends BaseEntity{
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "id", nullable = false)
//    private Integer testId;
    @Column(name = "testType", nullable = false)
    private String testType;
    @Column(name = "testDate", nullable = false)
    private Date testDate;
    @Column(name = "status", nullable = false, length = 500)
    private String status;
    @Column(name = "avScore",nullable = false)
    private double avScore;

/*    @OneToOne(mappedBy = "test", cascade = CascadeType.ALL)
    private Syllabus syllabus;*/

    @OneToMany(mappedBy = "test", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Quiz> quizzes = new ArrayList<>();

}
