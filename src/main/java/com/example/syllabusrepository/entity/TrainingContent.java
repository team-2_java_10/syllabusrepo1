package com.example.syllabusrepository.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "TrainingContent")
public class TrainingContent extends BaseEntity{
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "id", nullable = false)
//    private Long id;
    @Column(name = "name", nullable = false, length = 500)
    private String name;
    @Column(name = "outputStandard")
    private String outputStandard;
    @Column(name = "trainingTime")
    private int trainingTime;
    @Column(name = "deliveryType")
    private String deliveryType;
    @Column(name = "method", nullable = false)
    private String method;

    //private String unitCode;
/*    @OneToMany(mappedBy = "trainingContent", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<LearningObjective> learningObjectives = new ArrayList<>();*/


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "unit_id")
    private TrainingUnit trainingUnit;

    @OneToMany(mappedBy = "trainingContent", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<TrainingMaterials> trainingMaterials = new ArrayList<>();
}
