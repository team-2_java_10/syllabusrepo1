package com.example.syllabusrepository.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "trainingUnit")
public class TrainingUnit extends BaseEntity{
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "id", nullable = false)
//    private Long id;
//    @Column(name = "topic", nullable = false, length = 500)
//    private String topic;
//    @Column(name = "dayNumber", nullable = false)
//    private String dayNumber;
    @Column(name = "unitName", nullable = false)
    private String unitName;
    @Column(name = "unitNumber", nullable = false)
    private String unitNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "day_id")
    private DaySyllabus daySyllabus;


    @OneToMany(mappedBy = "trainingUnit", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<TrainingContent> trainingContents = new ArrayList<>();
}
