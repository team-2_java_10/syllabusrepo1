package com.example.syllabusrepository.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SyllabusDto extends BaseDto{
//    private Long id;
    //private String topicCode;
    private String syllabusName;
    private String syllabusCode;
    private int duration;
    private String version;
    private int attendeeNumber;
    private String technicalRequirement;
    //private String trainer;
    private String outputStandard;
    private String courseObjectives;
    private String level;
    public String publicStatus;
    public Integer trainingId;
}
