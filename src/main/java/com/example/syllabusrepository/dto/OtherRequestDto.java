package com.example.syllabusrepository.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OtherRequestDto {
    private Long id;
    private OtherSyllabusDto data;
}
