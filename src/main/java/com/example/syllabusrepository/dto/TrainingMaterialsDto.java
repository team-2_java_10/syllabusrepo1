package com.example.syllabusrepository.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TrainingMaterialsDto extends BaseDto{
//    private Long materialsId;
    private String title;
    private String s3Location;
    private byte[] trainingMaterialData;
}
