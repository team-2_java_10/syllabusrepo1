package com.example.syllabusrepository.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TrainingUnitDto extends BaseDto{
//    private Long id;
    //private String topic;
    //private String dayNumber;
    private String unitName;
    private String unitNumber;
    //private Syllabus syllabus;
    //private List<TrainingContentDto> trainingContent;
}
