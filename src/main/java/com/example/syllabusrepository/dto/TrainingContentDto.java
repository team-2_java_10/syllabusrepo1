package com.example.syllabusrepository.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TrainingContentDto extends BaseDto{
//    private Long id;
    private String name;
    private String outputStandard;
    private int trainingTime;
    private String deliveryType;
    private String method;
}
