package com.example.syllabusrepository.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AssignmentShemeDto extends BaseDto{
    private int assignment;
    private int quiz;
    private int finalSyllabus;
    private int finalTheory;
    private int finalPractice;
    private int gpa;
}
